
export let eingaben = {}

export default defineEventHandler(async (event) => {
    const body = await useBody(event)


    if(eingaben[body.quizname] == undefined)
        eingaben[body.quizname] = []
    if(eingaben[body.quizname][body.aktuelleFrage] == undefined)
        eingaben[body.quizname][body.aktuelleFrage]  = [[],[],[],[]]

    eingaben[body.quizname][body.aktuelleFrage][body.auswahl].push(body.spieler)

    return { eingaben }
})
