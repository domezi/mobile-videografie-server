import {eingaben} from "~/server/api/vote";


export default defineEventHandler(async (event) => {
    const {quizname} = await useBody(event)
    return eingaben[quizname] ?? {}
})
