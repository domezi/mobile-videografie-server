import fs from "fs";
import yaml from "js-yaml"
import {readdirSync} from "fs";

// zufällige antworten anordnen
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

// alle ordner mit quiz-dateien suchen
function getDirectories(source) {
    return readdirSync(source, {withFileTypes: true})
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name)
}

let quizzes = []

let directories = getDirectories('./public/quizzes/')

for(let directory of directories) {

    // alle fragen einlesen
    let fragen = fs.readFileSync("./public/quizzes/" + directory + "/fragen.yaml", "utf8")
    let quiz = shuffle(yaml.load(fragen))

    // richtige antworten einlesen
    quiz = quiz.map(({aufgabe, bild, antworten}) => ({
        aufgabe,
        bild,
        richtig: antworten[0],
        antworten: shuffle(antworten)
    }))

    quizzes.push(quiz)
}

export default defineEventHandler(async (event) => {
    const body = await useBody(event)
    return quizzes[body.quizname]

})
