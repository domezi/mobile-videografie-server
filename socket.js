const httpServer = require("http").createServer();
const io = require("socket.io")(httpServer, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});

io.on("connection", (socket) => {
    socket.on("aktuelleFrage", (aktuelleFrage) => {
        socket.broadcast.emit("aktuelleFrage", aktuelleFrage)
    })
    socket.on("richtigZeigen", (richtigZeigen) => {
        socket.broadcast.emit("richtigZeigen", richtigZeigen)
    })
    socket.on("abgestimmt", () => {
        socket.broadcast.emit("abgestimmt")
    })
    socket.on("present", () => {
        socket.broadcast.emit("present")
    })
});

httpServer.listen(3005);
