# Kahoot-like quiz game

start servers
```
yarn 
yarn build 
node .output/server/index.mjs &
nodemon socket.js
```

visit webpages
```
http://localhost:3000/
http://localhost:3000/quiz/1/present
http://localhost:3000/quiz/1/controller
```

add quizzes
- create folder /public/quizzes/1234/
- create /public/quizzes/1234/fragen.yml
