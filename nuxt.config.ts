import {defineNuxtConfig} from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    server: {
        host: '0.0.0.0',
        port: 80
    },
    css: ["@/assets/styles/main.sass"]
})
